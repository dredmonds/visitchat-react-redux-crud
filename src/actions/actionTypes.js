export const GET_ALL_ITEMS = 'GET_ALL_ITEMS';
export const CREATE_NEW_ITEM = 'CREATE_NEW_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
