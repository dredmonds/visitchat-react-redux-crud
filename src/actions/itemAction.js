import * as actionTypes from './actionTypes';

export const createItem = (item) => {
    return {
      type: actionTypes.CREATE_NEW_ITEM,
      item: item
    }
  };

export const deleteItem = (id) => {
    return {
        type: actionTypes.REMOVE_ITEM,
        id: id
    }
}
