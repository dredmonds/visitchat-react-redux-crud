import * as actionTypes from '../actions/actionTypes';

export default (state = [], action) => {
    switch (action.type){
      case actionTypes.CREATE_NEW_ITEM:
      return [
        ...state,
        Object.assign({}, action.item)
      ];
      case actionTypes.REMOVE_ITEM:
      return state.filter((data, i) => i !== action.id);
      default:
            return state;
    }
  };
