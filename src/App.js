import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as itemAction from './actions/itemAction';

class App extends Component {

  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      item: ''
    }
  }

  handleChange(e){
    this.setState({
      item: e.target.value
    })
  }

  handleSubmit(e){
    e.preventDefault();
    let data = {
      item: this.state.item
    }
    this.setState({
      item: ''
    });
    this.props.createItem(data);
  }

  listView(data, index){
    return (
      <div className="row">
        <div className="col-md-10">
          <li key={index} className="list-group-item clearfix">
            {data.item}
          </li>
        </div>
        <div className="col-md-2">
          <button onClick={(e) => this.deleteItem(e, index)} className="btn btn-danger">
            Remove
          </button>
        </div>
    </div>
    )
  }

  deleteItem(e, index){
    e.preventDefault();
    this.props.deleteItem(index);
  }

  render() {

    return(
      <div className="container">
        <h1>React + Redux CRUD Application</h1>
        <hr />
        <div>
          <h3>Add Item Form</h3>
          <form onSubmit={this.handleSubmit}>
            <input type="text" onChange={this.handleChange} className="form-control" value={this.state.item}/><br />
            <input type="submit" className="btn btn-success" value="ADD"/>
          </form>
          <hr />
        { <ul classitem="list-group">
          {this.props.items.map((item, i) => this.listView(item, i))}
        </ul> }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    items: state.items
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createItem: item => dispatch(itemAction.createItem(item)),
    deleteItem: index => dispatch(itemAction.deleteItem(index))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
