# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary:
A sample CRUD application using React + Redux
* Version:
v0.0.1

### How do I get set up? ###

* Summary of set up:
Git pull the project repository
* Configuration:
Install NodeJS, Yarn and Serve(npm install -g server) for localhost web publishing of "build" folder.
* Dependencies:
Please refer to project package.json
* How to run tests:
To be follows...
* Available Scripts:
 - $npm start -> to start project running at localhost:3000
 - $npm build -> build project for deployment
 - $npm eject -> exit project

### TODO'S ###
- [ ] user login
- [x] CRUD item
- [ ] user audit trail
- [ ] test script
- [ ] user level permission
- [ ] responsive view for web and mobile

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
